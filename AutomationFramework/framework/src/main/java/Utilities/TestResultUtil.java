package Utilities;

import static org.testng.Assert.assertTrue;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import dataobject.Artifact;
import properties.APIEnum;

public class TestResultUtil {

	public TestResultUtil() {
		// TODO Auto-generated constructor stub
	}

	public void printResultsToConsole(Artifact artifact, APIEnum... enums) {
		StringBuilder msg = new StringBuilder("Test Results\n");
		
		for(APIEnum api : enums) {
			msg.append(api.name()).append("").append("\n");
		}
		System.out.println(msg);
	}
	
	public String convertToCSV(String [] data) {
	return	Stream.of(data).map(this::escapeSpecialCharacters).collect(Collectors.joining(","));
	}
	
	private String escapeSpecialCharacters(String data) {
		String escapeData = data.replaceAll("\\R", "");
		if(data.contains(",")||data.contains("\"")||data.contains("'")) {
			data.replace("\"", "\"\"");
			escapeData= "\""+data+"\"";
		}
		return escapeData;
	}
	
	public void printResultsToFile(Artifact artifact, APIEnum... enums) {
		
		String pathName = "test-output/test_results.csv";
		File csvOutputFile = new File(pathName);
		String testPlan = Thread.currentThread().getStackTrace()[2].getClassName();
		String testCase = Thread.currentThread().getStackTrace()[2].getMethodName();
		String testData = artifact.getAccount().toString();
		try {
			BufferedWriter bw = new BufferedWriter(new FileWriter(csvOutputFile));
			List<String []>  dataLines = new ArrayList<>();
			dataLines.add(new String[] {""+new Date(System.currentTimeMillis())});
			dataLines.add(new String [] {testPlan,testCase,testData});
			String [] headers = {"StepName" ,"ID", "Response","Corr ID"};
			dataLines.add(headers);
			for(APIEnum api : enums) {
				String apiName = api.name();
				String ID = artifact.getSteps().get(apiName).getIdString("post");
				String Response = artifact.getSteps().get(apiName).getResponse("post");
				String corrID = artifact.getSteps().get(apiName).getResponse("post");
				
				String[] data = {apiName,ID,Response,corrID};
				dataLines.add(data);
			}
			dataLines.add(new String[] {"\n"});
			for(String [] dataLine:dataLines) {
				String s = convertToCSV(dataLine)+"\n";
				bw.write(s);
			}
			bw.close();
		}
		catch (Exception e) {
			throw new RuntimeException (e);
		}
		assertTrue(csvOutputFile.exists());
	}
}
