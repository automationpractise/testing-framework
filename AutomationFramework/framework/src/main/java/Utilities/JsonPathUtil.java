package Utilities;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import com.jayway.jsonpath.Configuration;
import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import com.jayway.jsonpath.Option;
import com.jayway.jsonpath.ReadContext;

import net.minidev.json.JSONArray;
import net.minidev.json.JSONObject;

public class JsonPathUtil {

 private static	DocumentContext jsonContext = null;
	public JsonPathUtil() {
		// TODO Auto-generated constructor stub
	}
	
	public static  String getJsonString(String jsonString, String path) {
		jsonContext = JsonPath.parse(jsonString);
		return jsonContext.read(path);
	}

	public static List getJsonNodes(String jsonString) {
		
		List nodes = new ArrayList<>();
		LinkedHashMap data = getJsonObject(jsonString,"$");
		Set set = getJsonObject(jsonString,"$").keySet();
		
		for(Object key : set) {
			nodes.add(key.toString());
		}
		return nodes;
	}
	
	public List <LinkedHashMap> getJsonArray(String jsonString, String path){
		jsonContext = JsonPath.parse(jsonString);
		return jsonContext.read(path);
	}
	
	
	// getJsonPath Value will return the json string based on query parameters json_obj and the path
	
	//jayway JSONPath evaluator can be found at http://jsonpath.herokuapp.com
	public static String getJsonValue(String json_obj, String path) {
		String value = null;
		Configuration conf = Configuration.defaultConfiguration().addOptions(Option.ALWAYS_RETURN_LIST);
		List <JSONArray> results = JsonPath.using(conf).parse(json_obj).read(path);
		
		try {
			if(results.size()>0) {
				value = JsonPath.parse(results.get(0)).jsonString();
			}
			else {
				// no results
			}
		}
		catch(Exception e) {
		value =	JsonPath.parse(results.get(0)).json().toString();
		}
		return value;
		
	}
	
	// getJsonObject will return the Linked hASHMAP OBJECT BASED on query parameters json_obj and the path
	public static LinkedHashMap getJsonObject(String json_obj, String path) {
		Configuration conf = Configuration.defaultConfiguration().addOptions(Option.ALWAYS_RETURN_LIST);
		List <LinkedHashMap> results = JsonPath.using(conf).parse(json_obj).read(path);
		LinkedHashMap value = null;
		if(results.size()>0) {
			value = results.get(0);
		}
		return value;
	}
	
	// methoid will put the key and new value into Json based on parametersjson_obj and path to JSON and return the updated json_obj
	public static String putJsonValue(String json_obj, String path, String key, String newValue) {
		Object data = JsonPath.read(newValue,"$");
		return JsonPath.parse(json_obj).put(path, key, data).jsonString();
	}
	
	// methoid will add  new value into Json Array based on parametersjson_obj and path to JSON array and return the updated json_obj
	
	public static String addJsonValue(String json_obj, String path,String newValue) {
		Object data = JsonPath.read(newValue,"$");
		return JsonPath.parse(json_obj).add(path, data).jsonString();
	}
	
	// methoid will set the new value into Json path  based on parametersjson_obj and path to JSON and return the updated json_obj
	
	public static String setJsonValue(String json_obj, String path,String newValue) {
		if(Objects.isNull(newValue)||newValue.length()==0) {
			newValue = "";
		}
		String data =JsonPath.read(newValue,"$").toString(); 
		return JsonPath.parse(json_obj).set(path, data).jsonString();
	}
	
	public static String readFromFile(String fileLoc) {
		String data = "";
		try {
			data = JsonPath.parse(new File(fileLoc)).jsonString();
		}catch(Exception e) {
			e.printStackTrace();
		}
		return data;
	}
	
	// gets Json object using json path anad checks for exist
	public static Boolean isJsonKeyExist(String json_obj, String path,String key ) {
		HashMap results = new HashMap();
		try {
			ReadContext parsedJson = JsonPath.parse(json_obj);
			results= parsedJson.read(path);
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		return results.containsKey(key);
	}
	
	public static String getJsonValueAsIs(String json_obj, String path) {
		String value = null;
		List<Object> results = JsonPath.parse(json_obj).read(path);
		try {
			if (results.size()>0) {
				value = JsonPath.parse(results).jsonString();
			}
		}catch (UnsupportedOperationException e) {
			results = JsonPath.read(json_obj, path);
			if(results.size()>0) {
				value = results.get(0).toString();
			}
		}
		return value;
	}
	
	public static String deletePath( String json_obj, String path) {
		return JsonPath.parse(json_obj).delete(path).jsonString();
	}
	
	public static String addEmptyJsonObject( String json_obj, String path, String key) {
		Object data = new JSONObject();
		//JsonObject
		
		return JsonPath.parse(json_obj).put(path, key, data).jsonString();
		
	}
}
