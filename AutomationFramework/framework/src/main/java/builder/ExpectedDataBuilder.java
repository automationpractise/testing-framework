package builder;

import java.util.HashMap;

public class ExpectedDataBuilder {
	HashMap <String,String> expectedData = new HashMap<>();
	
	public ExpectedDataBuilder add(String path, String value) {
		
		expectedData.put(path, value);
		return this;
	}
	
	public void build() {
		//return new ExpectedResponse(expectedData);
	}
}
