package dataobject;

public class APIResponseData {

	String id;
	
	String response;
	String corrid;
	public APIResponseData(String id, String Response, String corrid) {
		// TODO Auto-generated constructor stub
		this.id=id;
		this.response = Response;
		this.corrid= corrid;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getResponse() {
		return response;
	}
	public void setResponse(String response) {
		this.response = response;
	}
	public String getCorrid() {
		return corrid;
	}
	public void setCorrid(String corrid) {
		this.corrid = corrid;
	}
}
