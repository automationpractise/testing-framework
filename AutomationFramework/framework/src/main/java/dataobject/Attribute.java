package dataobject;

public class Attribute {

	String id;
	
	String response;
	public Attribute(String id, String response) {
		// TODO Auto-generated constructor stub
		this.id= id;
		this.response= response;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getResponse() {
		return response;
	}
	public void setResponse(String response) {
		this.response = response;
	}
}
