package dataobject;

import java.util.HashMap;

import Steps.API.StepBase;


public class Artifact {
private String hostname;
private HashMap<String, Object> headers ;
HashMap<String, StepBase> steps ;
String AcessToken;
HashMap<String, Attribute> attribute ;
Account account;

public Artifact() {
	this.hostname= getHostname();
	this.steps = new HashMap<>();
}


public String getHostname() {
	
	return hostname;
}
public void setHostname(String hostname) {
	this.hostname = hostname;
}
public HashMap<String, Object> getHeaders() {
	return headers;
}
public void setHeaders(HashMap<String, Object> headers) {
	this.headers = headers;
}
public HashMap<String, StepBase> getSteps() {
	return steps;
}
public void setSteps(HashMap<String, StepBase> steps) {
	this.steps = steps;
}
public String getAcessToken() {
	return AcessToken;
}
public void setAcessToken(String acessToken) {
	AcessToken = acessToken;
}
public HashMap<String, Attribute> getAttribute() {
	return attribute;
}
public void setAttribute(HashMap<String, Attribute> attribute) {
	this.attribute = attribute;
}
public Account getAccount() {
	return account;
}
public void setAccount(Account account) {
	this.account = account;
}

}
