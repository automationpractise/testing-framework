package Validator;

import java.util.HashMap;

public abstract class ResponseValidatorBuilderBase {

	protected HashMap<String,String> expectedData = new HashMap<>();
	
	public abstract IResponseValidator build();

}
