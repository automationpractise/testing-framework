package Validator;

import java.util.Map;

public class OrderValidator  extends ResponseValidatorBase{

	public OrderValidator(Map<String,String>expectedData) {
		// TODO Auto-generated constructor stub
		super(expectedData);
	}

	public static class OrderValidatorBuilder extends ResponseValidatorBuilderBase {
		
		public OrderValidatorBuilder intent(String type) {
			expectedData.put("$ intent", type);
			return this;
		}
		
		
		@Override
		public IResponseValidator build() {
			// TODO Auto-generated method stub
			return new OrderValidator(expectedData);
		}
	}

	
}
