package Validator;

import Response.Iresponse;

public interface IResponseValidator {
	void verify(Iresponse response);
	void Assertion(Iresponse response);
}
