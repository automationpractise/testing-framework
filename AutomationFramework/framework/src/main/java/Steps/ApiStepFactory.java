package Steps;

import Steps.API.OrderAPI;
import Steps.API.StepBase;
import Steps.UI.BasePage;

public class ApiStepFactory {

	/*
	 * public static <T extends StepBase> T getAPIStep (Class <T> type) { T page =
	 * null; try { page = type.getDeclaredConstructor().newInstance();
	 * 
	 * } catch(Exception e) { System.out.println("EXCEPTION : "+e.getMessage()); }
	 * return page; }
	 */
	public static  OrderAPI getOrderStep() {
		return new OrderAPI();
	}

}
