package Steps;

import Steps.UI.BasePage;

public class UIPageFactory {

		
	public static <T extends BasePage> T getPage (Class <T> type) {
		T page = null;
		try {
			page = type.getDeclaredConstructor().newInstance();
		}
		catch(Exception e) {
			System.out.println("EXCEPTION : "+e.getMessage());
		}
		return page;
	}

}
