package Steps.API;

import java.util.HashMap;
import java.util.Map;

import Steps.UI.BasePage;
import builder.HeaderBuilder;
import properties.APIEnum;

public class OrderAPI extends StepBase{
String flow ;
String path = "/login/API";
String name = APIEnum.LoginAPI.name();
	public OrderAPI() {
		super("","");
		// TODO Auto-generated constructor stub
		
	}
	
	public OrderAPI setFlow(String flow) {
		this.flow= flow;
		return this;
	}
	
 public OrderAPI getOrderAPI() {
	 if(flow.equals("V2")) {System.out.println("ORDER API V2");}
	 else {
	 System.out.println("ORDER API V1");
	 }
	 return this;
 }
 public HashMap<String, Object> getPostHeaders() {
	return new HeaderBuilder().addHeader("content", "json").addHeader("bearerToken", "5888").build();
 }
 
 public String getEndpoint() {
	 String endpoint = null;
	 if(operation.contains("post")) {
		 endpoint = "";
	 }
	 
	 return endpoint;
 }



@Override
public String getStepName() {
	// TODO Auto-generated method stub
	return name;
}
}
