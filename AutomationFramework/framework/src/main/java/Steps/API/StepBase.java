package Steps.API;

import java.util.HashMap;
import java.util.Map;

import Response.HttpStepResponse;
import Response.Iresponse;
import dataobject.APIResponseData;
import dataobject.Artifact;

public abstract class StepBase {

	String portNumber;
	String idPath;
	Map<String, APIResponseData> result ;
	String operation;
	public StepBase(String portnumber, String idPath) {
		this.portNumber = portnumber;
		this.idPath = idPath;
		// TODO Auto-generated constructor stub
	}
  public abstract HashMap<String, Object> getPostHeaders() ;
  public abstract String getStepName();
  public void setStepResult(String operation,String id, String response, String corrid) {
	  APIResponseData data = new APIResponseData(id,response,  corrid);
	  result.put(operation, data);
  }
  
  public String getResponse(String operation) {
	  return result.get(operation).getResponse();  }
  
  public String getIdString (String operation) {
	  return result.get(operation).getId();  }
  
  public String getCallerName() {
	  return Thread.currentThread().getStackTrace()[2].getMethodName();
  }
  
  public abstract String getEndpoint();
  public Iresponse post(Artifact artifact) {
	  operation = getCallerName();
	  String endPoint = getEndpoint();
	  Map<String, Object>   headers = getPostHeaders();
	  // implement post operation Rest Assured
	  String id = "";
	  String response = "";
	  String corrid="";
	  setStepResult(operation,id,response, corrid);
	  artifact.getSteps().put(getStepName(), this);
	  
	  return new HttpStepResponse(response);  }
}
