package Page_Factory;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Plp_PF {

	public Plp_PF(WebDriver driver) {
		// TODO Auto-generated constructor stub
		PageFactory.initElements(driver, this);
	}
	@FindBy(xpath ="//div[@class='a-row a-spacing-none']/a/h2")
	List<WebElement> result1 ;
	
	public List<WebElement> result1() { return result1;}
	
	@FindBy(id="add-to-cart-button")
	WebElement addcart;
	
	public WebElement addcart() {return addcart;}
}
