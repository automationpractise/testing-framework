package Page_Factory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Home_PF {

	public Home_PF(WebDriver driver) {
		// TODO Auto-generated constructor stub
		PageFactory.initElements(driver, this);
	}
	@FindBy(id="twotabsearchtextbox")
	WebElement searchbox;
	
	public WebElement searchbox() {return searchbox;}
	
	@FindBy( className="nav-input")
	WebElement Searchsubmit;
	
	public WebElement Searchsubmit() {return Searchsubmit; }
}
