package step_definitions;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import Page_Factory.Home_PF;
import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class search {
	WebDriver driver;
	hooks hooks;
	public search(hooks hook) throws IOException {
		//this.hooks=hook;
		driver=	hook.driver;
	}

	@Given("^User opens the application$")
	public void user_opens_the_application() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	   // throw new PendingException();
		driver.get("https://www.amazon.in/");
	}

	@When("^User clicked on header link$")
	public void user_clicked_on_headerlink() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    // For automatic transformation, change DataTable to one of
	    // List<YourType>, List<List<E>>, List<Map<K,V>> or Map<K,V>.
	    // E,K,V must be a scalar (String, Integer, Date, enum etc)
	   // throw new PendingException();
		//driver.findElement(By.id("twotabsearchtextbox")).sendKeys("bat");
		//driver.findElement(By.className("nav-input")).click();
		
		Home_PF home =new Home_PF(driver);
		home.searchbox().sendKeys("bat");
		home.Searchsubmit().click();
		//driver.findElement(By.linkText("Today's Deals")).click();
	}
	@Then("^Product should list based on user search$")
	public void product_should_list_based_on_user_search() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	 
		System.out.println("product list display") ;
		// throw new PendingException();
	}

}
