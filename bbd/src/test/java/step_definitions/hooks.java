package step_definitions;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.java.After;
import cucumber.api.java.Before;

public class hooks {
	
 static	WebDriver driver;
	
		@Before
		public void setup() throws IOException {
		String currentpath=System.getProperty("user.dir");
		
		System.out.println(currentpath);
			 System.setProperty("webdriver.chrome.driver", currentpath+"\\libs\\chromedriver.exe");

             driver=new ChromeDriver();
             driver.manage().window().maximize();
			
	// return driver;
	}
	
		
		@After
		public void teardown()
		{
			
			//driver.close();
		}
}
